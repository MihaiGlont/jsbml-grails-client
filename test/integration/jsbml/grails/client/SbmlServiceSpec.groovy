package jsbml.grails.client

import com.ctc.wstx.api.ReaderConfig
import com.ctc.wstx.stax.WstxInputFactory
import grails.test.mixin.TestFor
import javax.xml.stream.XMLInputFactory
import net.biomodels.jummp.plugins.sbml.SbmlService
import org.codehaus.stax2.XMLInputFactory2
import org.sbml.jsbml.SBMLDocument
import org.sbml.jsbml.SBMLReader
import spock.lang.*

@TestFor(SbmlService)
class SbmlServiceSpec extends Specification {
    void "BIOMD0000000412, BIOMD0000000272 and fbc_example1 load just fine but have consistency issues"() {
        when:
            File f = new File(location)
            SBMLDocument d = new SBMLReader().readSBML(f)
            int status = d.checkConsistency()
        then:
            f.exists()
            d != null
            d.getModel() != null
            status != 0
            println d.getListOfErrors().validationErrors.collect { it.getMessage() }

        where:
            location << ["BIOMD0000000412.xml", "fbc_example1.xml", "BIOMD0000000272.xml"]
    }

    @IgnoreRest
    void woodstoxTest() {
        when: "a factory is instantiated"
        WstxInputFactory factory = new WstxInputFactory()
        ReaderConfig cfg = factory.getConfig()
        int flags = cfg.getConfigFlags()
        ReaderConfig defaultConfig = ReaderConfig.createFullDefaults()
        int defaultFlags = defaultConfig.getConfigFlags()
        int propertyCheck = flags & 0x0100

        then: "it has the default configuration and hence PROLOG_WS is off"
        propertyCheck == 0
        defaultFlags == flags

        when: "PROLOG_WS is manually switched on"
        factory.setProperty(XMLInputFactory2.P_REPORT_PROLOG_WHITESPACE, Boolean.TRUE)
        flags = cfg.getConfigFlags()
        propertyCheck = flags & 0x0100

        then:
        0x0100 == propertyCheck

        when: "a factory with a custom Stax2 profile is created"
        factory = new WstxInputFactory()
        factory.configureForRoundTripping()
        cfg = factory.getConfig()
        flags = cfg.getConfigFlags()
        propertyCheck = flags & 0x0100

        then: "PROLOG_WS is switched on"
        256 == propertyCheck

        when: "a factory is created"
        factory = new WstxInputFactory()
        flags = factory.getConfig().getConfigFlags()
        propertyCheck = flags & 0x0100

        then: "it still has the default configuration"
        0 == propertyCheck

        when: "a factory is created via XMLInputFactory"
        factory = XMLInputFactory.newInstance()
        flags = factory.getConfig().getConfigFlags()
        propertyCheck = flags & 0x0100

        then: "PROLOG_WS is still off"
        0 == propertyCheck
    }

    void "SbmlL1V1 loads just fine"() {
        expect:
        File f = new File("SbmlL1V1.xml")
        f.exists()
        service != null

        when:
        def err = []
        SBMLDocument d = service.getFileAsValidatedSBMLDocument(f, err)

        then:
        f.exists()
        d != null
        d.getModel() != null
        err.isEmpty()

        when:
        int status = d.checkConsistency()

        then:
        status == 0

        when:
        WstxInputFactory factory = WstxInputFactory.newInstance()
        ReaderConfig cfg = factory.getConfig()
        int flags = cfg.getConfigFlags()
        int defaultFlags = ReaderConfig.createFullDefaults().getConfigFlags()
        int result = flags & 0x0100

        then:
        println flags
        defaultFlags == flags
        0 == result  // CFG_REPORT_PROLOG_WS is not set
    }

    void "BMID000000140325 and SbmlL1V1 load just fine"() {
        when:
            File f = new File(location)
            SBMLDocument d = new SBMLReader().readSBML(f)
        then:
            f.exists()
            d != null
            d.getModel() != null

        when:
            int status = d.checkConsistency()
        then:
            status == 0

        where:
        location << ["BMID000000140325.xml", "SbmlL1V1.xml"]
    }

    @Ignore
    void "Model descriptions are extracted properly"() {
        given:
            SbmlService service = new SbmlService()
        when:
            File f = new File("fbc_example1.xml")
            String desc = service.extractDescription([f])
        then:
            desc == ""

        when:
            f = new File(location)
            desc = service.extractDescription([f])
        then:
            expectedDescription == desc
        where:
            location << ["BMID000000140325.xml", "fbc_example1.xml", "BIOMD0000000272.xml", "BIOMD0000000412.xml"]
            expectedDescription << [
                    '''<notes xmlns="http://www.sbml.org/sbml/level2/version4">\r\n      <body xmlns="http://www.w3.org/1999/xhtml">\r\n        <div class="dc:title">Whole Genome Metabolism of "Ammonifex degensii (strain DSM 10501 / KC4)"</div>\r\n            <div class="dc:description">This is a whole genome metabolism model of Ammonifex degensii (strain DSM 10501 / KC4).</div>\r\n            <div class="dc:provenance">This model has been automatically generated by the      <a href="http://identifiers.org/pubmed/22095399" title="Access publication about: SuBliMinaL Toolbox">SuBliMinaL Toolbox</a>\r\n          and libAnnotationSBML using information coming from from KEGG (release 66, April 2013, accessed via the resource's web services interface) and, where relevant, augmented with metabolic pathway information extracted from MetaCyc (version 17.0, March 2013).      </div>\r\n            <div class="dc:publisher">This model has been produced by the      <a href="http://code.google.com/p/path2models/" title="Access to the path2models' website">path2models</a>\r\n          project and is currently hosted on      <a href="http://identifiers.org/pubmed/20587024" title="Access publication about: BioModels Database">BioModels Database</a>\r\n          and identified by:      <a href="http://identifiers.org/biomodels.db/BMID000000140325" title="Access to this model via BioModels Database">BMID000000140325</a>\r\n          .      </div>\r\n            <div class="dc:license">To the extent possible under law, all copyright and related or neighbouring rights to this encoded model have been dedicated to the public domain worldwide. Please refer to      <a href="http://creativecommons.org/publicdomain/zero/1.0/" title="Creative Commons CC0">CC0 Public Domain Dedication</a>\r\n          for more information.      </div>\r\n            </body>\r\n      \r\n    </notes>''',
                    "",
                    '''<notes xmlns="http://www.sbml.org/sbml/level2/version4">\r\n <body xmlns="http://www.w3.org/1999/xhtml">\r\n <p>This is the auxiliary model described in the article: <br />\r\n <strong>Covering a Broad Dynamic Range: Information Processing at the Erythropoietin Receptor</strong>\r\n <br />\r\n Verena Becker, Marcel Schilling, Julie Bachmann, Ute Baumann, Andreas Raue, Thomas Maiwald, Jens Timmer and Ursula Klingmüller; <em>Science</em>\r\n Published Online May 20, 2010; DOI: <a href="http://dx.doi.org/10.1126/science.1184913">10.1126/science.1184913</a>\r\n PMID: <a href="http://www.ncbi.nlm.nih.gov/pubmed/20488988">20488988</a>\r\n <br />\r\n Abstract: <br />\r\n Cell surface receptors convert extracellular cues into receptor activation, thereby triggering intracellular signaling networks and controlling cellular decisions. A major unresolved issue is the identification of receptor properties that critically determine processing of ligand-encoded information. We show by mathematical modeling of quantitative data and experimental validation that rapid ligand depletion and replenishment of cell surface receptor are characteristic features of the erythropoietin (Epo) receptor (EpoR). The amount of Epo-EpoR complexes and EpoR activation integrated over time corresponds linearly to ligand input, covering a broad range of ligand concentrations. This relation solely depends on EpoR turnover independent of ligand binding, suggesting an essential role of large intracellular receptor pools. These receptor properties enable the system to cope with basal and acute demand in the hematopoietic system. </p>\r\n <p>SBML model exported from PottersWheel.</p>\r\n <pre>% PottersWheel model definition file\r\n\r\nfunction m = BeckerSchilling2010_EpoR_AuxiliaryMode()\r\n\r\nm = pwGetEmptyModel();\r\n\r\n%% Meta information\r\n\r\nm.ID = 'BeckerSchilling2010_EpoR_AuxiliaryMode';\r\nm.name = 'BeckerSchilling2010_EpoR_AuxiliaryModel';\r\nm.description = 'BeckerSchilling2010_EpoR_AuxiliaryModel';\r\nm.authors = {'Verena Becker',' Marcel Schilling'};\r\nm.dates = {'2010'};\r\nm.type = 'PW-2-0-42';\r\n\r\n%% X: Dynamic variables\r\n% m = pwAddX(m, ID, startValue, type, minValue, maxValue, unit, compartment, name, description, typeOfStartValue)\r\n\r\nm = pwAddX(m, 'EpoR' , 76, 'fix' , 0, 10000, [], 'cell', [] , [] , [] , [] , 'protein.generic');\r\nm = pwAddX(m, 'SAv' , 999.293, 'global', 900, 1100, [], 'cell', [] , [] , [] , [] , 'protein.generic');\r\nm = pwAddX(m, 'SAv_EpoR' , 0, 'fix' , 0, 10000, [], 'cell', [] , [] , [] , [] , 'protein.generic');\r\nm = pwAddX(m, 'SAv_EpoRi', 0, 'fix' , 0, 10000, [], 'cell', [] , [] , [] , [] , 'protein.generic');\r\nm = pwAddX(m, 'dSAvi' , 0, 'fix' , 0, 10000, [], 'cell', [] , [] , [] , [] , 'protein.generic');\r\nm = pwAddX(m, 'dSAve' , 0, 'fix' , 0, 10000, [], 'cell', [] , [] , [] , [] , 'protein.generic');\r\n\r\n\r\n%% R: Reactions\r\n% m = pwAddR(m, reactants, products, modifiers, type, options, rateSignature, parameters, description, ID, name, fast, compartments, parameterTrunks, designerPropsR, stoichiometry, reversible)\r\n\r\nm = pwAddR(m, { }, {'EpoR' }, { }, 'C' , [] , 'k1*k2', {'kt','Bmax_SAv'}, [], 'reaction0001');\r\nm = pwAddR(m, {'EpoR' }, { }, { }, 'MA', [] , [] , {'kt' }, [], 'reaction0002');\r\nm = pwAddR(m, {'SAv','EpoR'}, {'SAv_EpoR' }, { }, 'MA', [] , [] , {'kon_SAv' }, [], 'reaction0003');\r\nm = pwAddR(m, {'SAv_EpoR' }, {'SAv','EpoR'}, { }, 'MA', [] , [] , {'koff_SAv' }, [], 'reaction0004');\r\nm = pwAddR(m, {'SAv_EpoR' }, {'SAv_EpoRi' }, { }, 'MA', [] , [] , {'kt' }, [], 'reaction0005');\r\nm = pwAddR(m, {'SAv_EpoRi' }, {'SAv' }, { }, 'MA', [] , [] , {'kex_SAv' }, [], 'reaction0006');\r\nm = pwAddR(m, {'SAv_EpoRi' }, {'dSAvi' }, { }, 'MA', [] , [] , {'kdi' }, [], 'reaction0007');\r\nm = pwAddR(m, {'SAv_EpoRi' }, {'dSAve' }, { }, 'MA', [] , [] , {'kde' }, [], 'reaction0008');\r\n\r\n\r\n\r\n%% C: Compartments\r\n% m = pwAddC(m, ID, size, outside, spatialDimensions, name, unit, constant)\r\n\r\nm = pwAddC(m, 'cell', 1);\r\n\r\n\r\n%% K: Dynamical parameters\r\n% m = pwAddK(m, ID, value, type, minValue, maxValue, unit, name, description)\r\n\r\nm = pwAddK(m, 'kt' , 0.0329366 , 'global', 1e-007, 1000);\r\nm = pwAddK(m, 'Bmax_SAv', 76 , 'fix' , 61 , 91 );\r\nm = pwAddK(m, 'kon_SAv' , 2.29402e-006, 'global', 1e-007, 1000);\r\nm = pwAddK(m, 'koff_SAv', 0.00679946 , 'global', 1e-007, 1000);\r\nm = pwAddK(m, 'kex_SAv' , 0.01101 , 'global', 1e-007, 1000);\r\nm = pwAddK(m, 'kdi' , 0.00317871 , 'global', 1e-007, 1000);\r\nm = pwAddK(m, 'kde' , 0.0164042 , 'global', 1e-007, 1000);\r\n\r\n\r\n%% Default sampling time points\r\nm.t = 0:3:99;\r\n\r\n\r\n%% Y: Observables\r\n% m = pwAddY(m, rhs, ID, scalingParameter, errorModel, noiseType, unit, name, description, alternativeIDs, designerProps)\r\n\r\nm = pwAddY(m, 'SAv + dSAve' , 'SAv_extracellular_obs');\r\nm = pwAddY(m, 'SAv_EpoR' , 'SAv_cellsurface_obs' );\r\nm = pwAddY(m, 'SAv_EpoRi + dSAvi', 'SAv_intracellular_obs');\r\n\r\n\r\n%% S: Scaling parameters\r\n% m = pwAddS(m, ID, value, type, minValue, maxValue, unit, name, description)\r\n\r\nm = pwAddS(m, 'scale_SAv_extracellular_obs', 1, 'fix', 0, 100);\r\nm = pwAddS(m, 'scale_SAv_cellsurface_obs' , 1, 'fix', 0, 100);\r\nm = pwAddS(m, 'scale_SAv_intracellular_obs', 1, 'fix', 0, 100);\r\n\r\n\r\n%% Designer properties (do not modify)\r\nm.designerPropsM = [1 1 1 0 0 0 400 250 600 400 1 1 1 0 0 0 0];</pre>\r\n <p>This model originates from BioModels Database: A Database of Annotated Published Models. It is copyright (c) 2005-2010 The BioModels.net Team. <br />\r\n For more information see the <a href="http://www.ebi.ac.uk/biomodels/legal.html" target="_blank">terms of use</a>\r\n . <br />\r\n To cite BioModels Database, please use: <a href="http://www.ncbi.nlm.nih.gov/pubmed/20587024" target="_blank">Li C, Donizelli M, Rodriguez N, Dharuri H, Endler L, Chelliah V, Li L, He E, Henry A, Stefan MI, Snoep JL, Hucka M, Le Novère N, Laibe C (2010) BioModels Database: An enhanced, curated and annotated resource for published quantitative kinetic models. BMC Syst Biol., 4:92.</a>\r\n </p>\r\n </body>\r\n \r\n </notes>''',
                    '''<notes xmlns="http://www.sbml.org/sbml/level2/version4">\r\n <body xmlns="http://www.w3.org/1999/xhtml">\r\n <p>This model is from the article: <br />\r\n <strong>The clock gene circuit in Arabidopsis includes a repressilator with additional feedback loops</strong>\r\n <br />\r\n Pokhilko A, Fernández AP, Edwards KD, Southern MM, Halliday KJ, Millar AJ. <em>Mol Syst Biol.</em>\r\n 2012 Mar 6;8:574. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22395476">22395476</a>\r\n , <br />\r\n <strong>Abstract:</strong>\r\n <br />\r\n Circadian clocks synchronise biological processes with the day/night cycle, using molecular mechanisms that include interlocked, transcriptional feedback loops. Recent experiments identified the evening complex (EC) as a repressor that can be essential for gene expression rhythms in plants. Integrating the EC components in this role significantly alters our mechanistic, mathematical model of the clock gene circuit. Negative autoregulation of the EC genes constitutes the clock's evening loop, replacing the hypothetical component Y. The EC explains our earlier conjecture that the morning gene PSEUDO-RESPONSE REGULATOR 9 was repressed by an evening gene, previously identified with TIMING OF CAB EXPRESSION1 (TOC1). Our computational analysis suggests that TOC1 is a repressor of the morning genes LATE ELONGATED HYPOCOTYL and CIRCADIAN CLOCK ASSOCIATED1 rather than an activator as first conceived. This removes the necessity for the unknown component X (or TOC1mod) from previous clock models. As well as matching timeseries and phase-response data, the model provides a new conceptual framework for the plant clock that includes a three-component repressilator circuit in its complex structure. </p>\r\n </body>\r\n \r\n </notes>'''
            ]
    }
}
